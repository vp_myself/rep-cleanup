import requests
import json
 
 
def makeRequest(method, url, auth=None, headers={}, json={}, verify=None, timeout=None):
  _verify = False
  if verify is not None:
    _verify = verify
  _timeout = 30
  if timeout is not None:
    _timeout = timeout
  try:
    if method.lower() == "get":
      return requests.get(url, auth=auth, json=json, verify=_verify, timeout=_timeout, headers=headers)
    elif method.lower() == "post":
      return requests.post(url,auth=auth, json=json, verify=_verify, timeout=_timeout, headers=headers)
    elif method.lower() == "put":
      return requests.put(url,auth=auth, json=json, verify=_verify, timeout=_timeout, headers=headers)
    elif method.lower() == "delete":
      return requests.delete(url,auth=auth, json=json, verify=_verify, timeout=_timeout, headers=headers)
  except requests.exceptions.Timeout:
    print('Request to %s Timed Out, exceeding %d seconds' % (url, _timeout))
  except requests.exceptions.RequestException as e:
    print("Config: Request to %s has exception %s" % (url, e))
  return None
