from utils import makeRequest
import argparse, sys
import re
import collections
from itertools import groupby
import datetime
import os
import json
 
INDEX_PATTERN = {
  "name": r"(.*)-(\d{4}(\.\d{2}(\.\d{2}){0,1}){0,1})$",
  "year": r"(.*-\d{4})(\.\d{2}(\.\d{2}){0,1})$",
  "month": r"(.*-\d{4}\.\d{2})(\.\d{2})$"
}
 
UNIT_PRIORITY = {
  "gb": 4,
  "mb": 3,
  "kb": 2,
  "b": 1
}
 
outputs = {
  "indicesTouched": None,
  "outputPatterns": None,
  "errors": None
}
 
os.environ['NO_PROXY'] = 'elastic.server:9243'
 
parser=argparse.ArgumentParser()
parser.add_argument('--es_serv', help='Elasticsearch server')
parser.add_argument('--uname', help='Username')
parser.add_argument('--pwd', help='Password')
parser.add_argument('--size', help='Size of indices to check (eg. 100kb, 10mb, ...). Format needs to be 1-3 digits and 2 chars, all lowercase')
parser.add_argument('--cleanupTo', help='value to define to what level the script will cleanup to. value: month/year/name')
parser.add_argument('--dryRun', help='run without performing any actions')
parser.add_argument('--numIndices', help='number of indices to perform dry-run/reindex on. Without this argument, the script will try to go through all indices')
parser.add_argument('--outFolder', help='Folder to store output files. Default is set to "."')
parser.add_argument('--indexPattern', help='ES index pattern that of indices that you want to reindex. Eg, test*')
 
args=parser.parse_args()
 
server = args.es_serv
 
if server is None:
  print("Server is not defined. Please specify --es_serv")
  exit()
 
uname = args.uname
pwd = args.pwd
sizeStr = args.size if args.size is not None else "1mb"
reindexTo = args.cleanupTo if args.cleanupTo is not None else "month"
dryRun = True if str.lower(args.dryRun) == "true" else False
numIndices = int(args.numIndices) if args.numIndices is not None else None
outFolder = "./output" if args.outFolder is None else args.outFolder
indexPattern = "*" if args.indexPattern is None else args.indexPattern
 
m = re.search(r'([\d\.]+)(\w+)', sizeStr)
markSize = m.group(1)
markUnit = m.group(2)
 
def main():
#  url = "%s/_cat/indices/%s,-.*?v&s=pri:desc,index:asc" % (server, indexPattern)
  url = "%s/_cat/indices/%s?v&s=pri:desc,index:asc" % (server, indexPattern)
  res = makeRequest("get", url, auth=(uname, pwd))
  rows = res.text.split("\n")
  rows = list(filter(None, rows))
 
  if len(rows) == 0:
    print("No indices detected.")
    exit()
 
  headings = rows[0].split()
  indexNames = []
  numRows = len(rows)
 
  loopUntil = 1
  if numIndices is None or (numIndices + 1) > numRows:
    loopUntil = numRows
  else:
    loopUntil = numIndices + 1
  for i in range(1, loopUntil):
    current = rows[i]
    currentArr = current.split()
    indexName = None
    index = {}
 
    for j in range(0, len(headings)):
      if len(currentArr) > 0:
        index[headings[j]] = currentArr[j]
 
      if headings[j] == "index":
        indexName = currentArr[j]
 
    if selectedSize(index["pri.store.size"]):
      if indexName is not None and re.search(INDEX_PATTERN[reindexTo], indexName) is not None:
        indexNames.append(indexName)
 
  selectedDict = {}
  for k, g in groupby(indexNames, lambda x: getIndexPattern(x)):
    t_group = list(g)
    #if len(t_group) > 1:
    selectedDict[k] = t_group
 
  print("\nIndex patterns:\n")
  for k, v in selectedDict.items():
    print(k)
    for item in v:
      print("\t%s" % (item))
    print("")
 
  print("Reindexing now ...\n")
 
  #create outFolder if not exists
  if not os.path.exists(outFolder):
    os.makedirs(outFolder)

  #open files
  for k, v in outputs.items():
    outputs[k] = open(("%s/%s.%s" % (outFolder, k, str(datetime.datetime.now().date()))), "a")
 
  for pattern, indices in selectedDict.items(): 
    for index in indices:
      print("\t%s => %s" % (index, pattern))

      if dryRun:
        print("\t...Skipping actual request due to dryRun.\n")
        continue

      reindexUrl = "%s/_reindex?wait_for_completion=false" % (server)
      reindexBod = {
        "source": {
          "index": index
        },
        "dest": {
          "index": pattern
        }
      }
      
      res = makeRequest("post" , reindexUrl, auth=(uname, pwd), json=reindexBod)
 
      if res is None:
        print("%s - Failed to reindex - src: %s" % (str(datetime.datetime.now()), index), file=outputs["errors"])
        print("%s - Failed to reindex - dest: %s" % (str(datetime.datetime.now()), pattern), file=outputs["errors"])
      else:
        print("%s - Reindexed - src: %s" % (str(datetime.datetime.now()), index), file=outputs["indices"])
        print("%s - Reindexed - dest: %s - task: %s" % (str(datetime.datetime.now()), pattern, json.loads(res.text)["task"]), file=outputs["patterns"])
  
  #close files in outFolder
  for k, v in outputs.items():
    outputs[k].close()
 
  print("Done\n")
 
#End of main()
 
def selectedSize(sizeStrToCheck):
  selected = True
  m = re.search(r'([\d\.]+)(\w+)', sizeStrToCheck)
  unitSize = m.group(1)
  unit = m.group(2)
 
  if UNIT_PRIORITY[unit] > UNIT_PRIORITY[markUnit]:
    selected = False
  if UNIT_PRIORITY[unit] == UNIT_PRIORITY[markUnit]:
    if float(unitSize) > float(markSize):
      selected = False
  return selected
 
def getIndexPattern(index):
  indexPattern = INDEX_PATTERN[reindexTo]
  grps = re.compile(indexPattern).split(index)
  if len(grps) > 1:
    return grps[1]
  else:
    return grps[0]
 
if __name__ == "__main__":
  main()